﻿using Newtonsoft.Json;

namespace Inventory.DAL.Models.Response
{
	public class MarketPriceResponse
	{
		[JsonProperty("success")]
		public bool Success { get; set; }
		[JsonProperty("lowest_price")]
		public string LowestPrice { get; set; }
		[JsonProperty("volume")]
		public float Volume { get; set; }
		[JsonProperty("median_price")]
		public string MedianPrice { get; set; }

		public override string ToString()
		{
			return $"success:{Success}, LowesPrice: {LowestPrice}, Volume: {Volume}, MedianPrice: {MedianPrice}";
		}
	}
}
