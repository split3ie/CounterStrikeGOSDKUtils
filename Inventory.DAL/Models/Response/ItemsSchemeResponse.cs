﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Inventory.DAL.Models.Response
{
	public class ItemsSchemeResponse
    {
        [JsonProperty("status")]
        public bool Status { get; set; }
        [JsonProperty("items_game_url")]
        public string ItemsUrl { get; set; }
        [JsonProperty("qualities")]
        public Dictionary<string, int> Qualities { get; set; }
        [JsonProperty("originNames")]
        public List<OriginItem> OriginNames { get; set; }
        [JsonProperty("items")]
        public List<MarketItem> Items { get; set; }
    }

    public class OriginItem
    {
        public string Name { get; set; }
        public int Origin { get; set; }
    }

    public class MarketItem
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("defindex")]
        public int DefIndex { get; set; }
        [JsonProperty("item_class")]
        public string Class { get; set; }
        [JsonProperty("item_type_name")]
        public string Type { get; set; }
        [JsonProperty("item_description")]
        public string Description { get; set; }
        [JsonProperty("proper_name")]
        public bool ProperName { get; set; }
        [JsonProperty("item_quality")]
        public float Quality { get; set; }
        [JsonProperty("image_url")]
        public string ImageUrl { get; set; }
        [JsonProperty("image_url_large")]
        public string ImageUrlLarge { get; set; }
        [JsonProperty("craft_class")]
        public string CraftClass { get; set; }
    }
}
