﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Inventory.DAL.Models.Response
{
	public class PlayerCsgoInventoryResponse
	{
		[JsonProperty("success")]
		public bool Success { get; set; }
		[JsonProperty("rgInventory")]
		public Dictionary<string, PlayerInventory> Inventory { get; set; }
		[JsonProperty("rgDescriptions")]
		public Dictionary<string, PlayerItemDetail> Details { get; set; }
	}
	public class PlayerInventory
	{
		[JsonProperty("id")]
		public long Id { get; set; }
		[JsonProperty("classid")]
		public long ClassId { get; set; }
		[JsonProperty("instanceid")]
		public long InstanceId { get; set; }
		[JsonProperty("amount")]
		public long Amount { get; set; }
		[JsonProperty("pos")]
		public long Position { get; set; }
	}
	public class PlayerItemDetail
	{
		[JsonProperty("appid")]
		public long ApplicationId { get; set; }
		[JsonProperty("classid")]
		public long ClassId { get; set; }
		[JsonProperty("instanceid")]
		public long InstanceId { get; set; }
		[JsonProperty("icon_url")]
		public string IconUrl { get; set; }
		[JsonProperty("icon_drag_url")]
		public string IconDragUrl { get; set; }
		[JsonProperty("name")]
		public string Name { get; set; }
		[JsonProperty("market_hash_name")]
		public string MarketHashName { get; set; }
		[JsonProperty("market_name")]
		public string MarketName { get; set; }
		[JsonProperty("type")]
		public string Type { get; set; }
		[JsonProperty("tradable")]
		public bool IsTradable { get; set; }
		[JsonProperty("marketable")]
		public bool IsMarketable { get; set; }
		[JsonProperty("commodity")]
		public bool IsCommodity { get; set; }
		[JsonProperty("market_tradable_restriction")]
		public long MarketTradableRestriction { get; set; }

	}
}
