﻿using Inventory.DAL.Constants;

namespace Inventory.DAL.Models.Request
{

	public class ItemSchemaRequest : BaseRequest
	{
		public ItemSchemaRequest(string apiKey, RestMethods method = RestMethods.GET) : base(method, false)
		{
			base.SetResource(string.Format(ApiActionUrls.GetSchema, apiKey));
		}
	}
}
