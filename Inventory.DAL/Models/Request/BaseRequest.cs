﻿using Inventory.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Inventory.DAL.Models.Request
{
	public abstract class BaseRequest : IApiRequest
	{
		public string Resource { get; private set; }
		public RestMethods Method { get; private set; }

		public bool UseSteamUrl { get; private set; }

		public BaseRequest(RestMethods method, bool useSteamUrl)
		{
			Method = method;
			UseSteamUrl = useSteamUrl;
		}
		public void SetResource(string resource)
		{
			Resource = resource;
		}

		public override string ToString()
		{
			return $"Resource: '{Resource}'; Method: '{Method.ToString()}'; UseSteamUrl: '{UseSteamUrl}'";
		}

		public string GetParams()
		{
			return ToString();
		}
	}
}
