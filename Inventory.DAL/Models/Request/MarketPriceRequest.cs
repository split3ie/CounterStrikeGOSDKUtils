﻿using Inventory.DAL.Constants;

namespace Inventory.DAL.Models.Request
{
	public class MarketPriceRequest : BaseRequest
	{
		public MarketPriceRequest(int currencyId, string marketHashName, RestMethods method = RestMethods.GET) : base(method, true)
		{
			SetResource(string.Format(ApiActionUrls.MarketPrice, currencyId, marketHashName));
		}
	}
}
