﻿using Inventory.DAL.Constants;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Inventory.DAL.Models.Request
{
	public class PlayerCsgoInventoryRequest : BaseRequest
	{
		public PlayerCsgoInventoryRequest(string playerId, RestMethods method = RestMethods.GET) : base(method, true)
		{
			Regex regex = new Regex(@"[\d{17}]");

			var idParam = "id";
			if (regex.IsMatch(playerId))
				idParam = "profiles";
			base.SetResource(string.Format(ApiActionUrls.PlayerCSGOInventory, idParam, playerId));
		}
	}
}
