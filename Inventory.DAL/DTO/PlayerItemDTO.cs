﻿namespace Inventory.DAL.DTO
{
	public class PlayerItemDTO
	{
		public int ApplicationId { get; set; }
		public long Amount { get; set; }
		public string IconUrl { get; set; }
		public string Name { get; set; }
		public string MarketHashName { get; set; }
		public string Type { get; set; }
		public bool IsTradable { get; set; }
		public bool IsMarketable { get; set; }
		public bool IsCommodity { get; set; }
		public long MarketTradableRestriction { get; set; }
		public int PlayerId { get; set; }
	}
}
