﻿namespace Inventory.DAL.Interfaces
{
	public interface IApiRequest
    {
        string Resource { get;}
        RestMethods Method { get; }
		bool UseSteamUrl { get; }
		string GetParams();
    }
}
