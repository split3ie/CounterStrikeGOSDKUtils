﻿using Core.Configuration.Interfaces;
using Inventory.DAL.Constants;
using Inventory.DAL.EntityFramework.DomainModels;
using Microsoft.EntityFrameworkCore;

namespace Inventory.DAL.EntityFramework
{
	public class InventoryDbContext : DbContext
	{
		public DbSet<Log> Logs { get; set; }
		public DbSet<Player> Players { get; set; }
		public DbSet<PlayerItem> PlayerItems { get; set; }
		public DbSet<Setting> Settings { get; set; }
		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseSqlite($"Data Source={ApplicationConstants.InventoryDbConnectionString}");
		}
	}

}
