﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inventory.DAL.EntityFramework
{
	public enum LogLevels
	{
		Trace,
		Debug,
		Info,
		Warn,
		Error,
		Fatal,
		Off
	}
}
