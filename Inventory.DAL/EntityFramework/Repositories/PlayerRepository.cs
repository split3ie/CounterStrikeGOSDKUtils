﻿using Inventory.DAL.EntityFramework.DomainModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.DAL.EntityFramework.Repositories
{
	public class PlayerRepository : BaseRepository<Player>
	{
		public PlayerRepository(DbContext context) : base(context)
		{
		}

		public Player GetPlayer(string profileId)
		{
			return DbSet.Include(x => x.Items).FirstOrDefault(x => x.ProfileId == profileId);
		}
	}
}
