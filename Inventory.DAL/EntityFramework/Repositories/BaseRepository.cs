﻿using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Inventory.DAL.EntityFramework.Repositories
{
	public abstract class BaseRepository<T> where T : class
	{
		protected DbContext DbContext;
		protected DbSet<T> DbSet;
		public BaseRepository(DbContext context)
		{
			DbContext = context;
			DbSet = DbContext.Set<T>();
		}

		public void Add(T entity)
		{
			DbSet.Add(entity);
		}
		public void Remove(T entity)
		{
			DbSet.Remove(entity);
			Commit();
		}
		public void Update(T entity)
		{
			var attach = DbSet.Attach(entity);
			attach.State = EntityState.Modified;
			DbSet.Update(entity);
		}
		public IQueryable<T> GetAll()
		{
			return DbSet.AsQueryable();
		}

		public void Commit()
		{
			DbContext.SaveChanges();
		}

		public void Save(T entity)
		{
			Add(entity);
			Commit();
		}
	}
}
