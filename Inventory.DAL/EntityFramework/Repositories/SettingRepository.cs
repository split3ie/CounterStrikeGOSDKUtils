﻿using Inventory.DAL.EntityFramework.DomainModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.DAL.EntityFramework.Repositories
{
	public class SettingRepository : BaseRepository<Setting>
	{
		public SettingRepository(DbContext context) : base(context)
		{
		}

		public string GetString(string key)
		{
			return GetAll().FirstOrDefault(x => x.Name == key)?.Value;
		}
		public int GetInt(string key)
		{
			var value = GetAll().FirstOrDefault(x => x.Name == key)?.Value;
			if (int.TryParse(value, out int result))
				return result;
			return default(int);
		}
		public float GetFloat(string key)
		{
			var value = GetAll().FirstOrDefault(x => x.Name == key)?.Value;
			if (float.TryParse(value, out float result))
				return result;
			return default(float);
		}
		public bool GetBool(string key)
		{
			var value = GetAll().FirstOrDefault(x => x.Name == key)?.Value;
			if (bool.TryParse(value, out bool result))
				return result;
			return default(bool);
		}
	}
}
