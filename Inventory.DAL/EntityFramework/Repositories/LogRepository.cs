﻿using Inventory.DAL.EntityFramework.DomainModels;
using Microsoft.EntityFrameworkCore;

namespace Inventory.DAL.EntityFramework.Repositories
{
	public class LogRepository : BaseRepository<Log>
	{
		public LogRepository(DbContext context) : base(context)
		{
		}
		public void Write(Log log)
		{
			base.Add(log);
			base.Commit();
		}
	}
}
