﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inventory.DAL.EntityFramework.DomainModels
{
	public class PlayerItem
	{
		[Key]
		public long ClassId { get; set; }
		public int ApplicationId { get; set; }

		public long Amount { get; set; }
		public string IconUrl { get; set; }
		public string Name { get; set; }
		public string MarketHashName { get; set; }
		public string Type { get; set; }
		public bool IsTradable { get; set; }
		public bool IsMarketable { get; set; }
		public bool IsCommodity { get; set; }
		public long MarketTradableRestriction { get; set; }

		public int PlayerId { get; set; }
		[ForeignKey(nameof(PlayerId))]
		public virtual Player Player { get; set; }
	}
}
