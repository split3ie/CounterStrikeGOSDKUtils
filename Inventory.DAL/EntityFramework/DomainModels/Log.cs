﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Inventory.DAL.EntityFramework.DomainModels
{
	public class Log
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long Id { get; set; }
		public DateTime Time { get; set; }
		public LogLevels Level { get; set; }
		public string Message { get; set; }
		public string AdditionalInfo { get; set; }
	}
}
