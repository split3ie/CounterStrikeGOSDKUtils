﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Inventory.DAL.EntityFramework.DomainModels
{
	public class Player
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Id { get; set; }
		public string ProfileId { get; set; }
		public DateTime LastModified { get; set; }
		public List<PlayerItem> Items { get; set; }
	}
}
