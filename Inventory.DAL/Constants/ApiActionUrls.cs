﻿namespace Inventory.DAL.Constants
{
	public class ApiActionUrls
	{
		public const string Market = "market/search/render/?query={0}&start={1}&count={2}&norender={3}";
		public const string GetSchema = "IEconItems_730/GetSchema/v2?key={0}";

		//{0} can be only id (when custom use link) or profiles (when steam id)
		public const string PlayerCSGOInventory = "{0}/{1}/inventory/json/730/2/";
		public const string MarketPrice = "market/priceoverview/?currency={0}&appid=730&market_hash_name={1}";

	}
}
