﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inventory.DAL.Constants
{
	public static class ApplicationConstants
	{
		public static readonly string InventoryDbConnectionString = $"{System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\\InvTools\\db\\InventoryDb.sqlite";
	}
}
