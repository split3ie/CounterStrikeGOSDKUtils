﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HammerUnitsConverter.Logic
{
    public static class Extensions
    {
        public static string GetString(this List<ConvertedValues> list)
        {
            var result = new StringBuilder();
            foreach(var item in list)
            {
                result.AppendLine($"{item.Units}|{item.Cm}|{item.M}|{item.Feet}|{item.Inches}|{item.Source}");
            }
            return result.ToString();
        }
    }
}
