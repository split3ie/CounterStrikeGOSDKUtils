﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HammerUnitsConverter.Logic
{
    public enum Source
    {
        Unit = 0,
        Cm,
        Inch,
        M,
        Foot
    }
}
