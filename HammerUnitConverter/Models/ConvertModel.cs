﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HammerUnitsConverter.Logic
{
    public class ConvertedValues
    {
        public float Units { get; set; }
        public float Cm { get; set; }
        public float Inches { get; set; }
        public float Feet { get; set; }
        public float M { get; set; }
        public Source Source { get; set; }
        public bool IsValid()
        {
            return Units > 0 || Cm > 0 || Inches > 0 || Feet > 0 || M > 0;
        }
    }
}
