﻿using HammerUnitsConverter.Logic;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace HammerUnitsConverter
{
    public partial class MainWindow : Form
    {
        private UnitConverter Converter = new UnitConverter();
        private readonly string DeveloperUrl = @"https://steamcommunity.com/id/The_Split/";
        private readonly string HelpUrl = @"https://developer.valvesoftware.com/wiki/Dimensions";

        private const string HistoryPath = @"hstr";
        public MainWindow()
        {
            InitializeComponent();
            historyDataGrid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        private void convertButton_Click(object sender, EventArgs e)
        {
            ConvertValues();
        }

        private void ConvertValues()
        {
            var convertModel = new ConvertedValues();
            if (unitsRadioButton.Checked)
            {
                convertModel.Units = !string.IsNullOrEmpty(unitsTextBox.Text) ? float.Parse(unitsTextBox.Text) : 0;
                convertModel.Source = Source.Unit;
            }
            else if (cmRadioButton.Checked)
            {
                convertModel.Cm = !string.IsNullOrEmpty(cmTextBox.Text) ? float.Parse(cmTextBox.Text) : 0;
                convertModel.Source = Source.Cm;
            }
            else if (mRadioButton.Checked)
            {
                convertModel.M = !string.IsNullOrEmpty(mTextBox.Text) ? float.Parse(mTextBox.Text) : 0;
                convertModel.Source = Source.M;
            }
            else if (inchesRadioButton.Checked)
            {
                convertModel.Inches = !string.IsNullOrEmpty(inchesTextBox.Text) ? float.Parse(inchesTextBox.Text) : 0;
                convertModel.Source = Source.Inch;
            }
            else if (footRadioButton.Checked)
            {
                convertModel.Feet = !string.IsNullOrEmpty(footTextBox.Text) ? float.Parse(footTextBox.Text) : 0;
                convertModel.Source = Source.Foot;
            }

            var result = Converter.DoConvert(convertModel);
            if (result != null && result.IsValid())
            {
                RefreshView(result);
            }

            RefreshGrid();
        }

        private void RefreshGrid()
        {
            var bindingSource = new BindingSource();

            if (Converter.History.Count == 0)
            {
                historyDataGrid.DataSource = bindingSource;
                return;
            }
            Converter.History.ForEach(x => bindingSource.Add(x));

            historyDataGrid.DataSource = bindingSource;

            DataGridViewCellStyle style = new DataGridViewCellStyle();
            style.Font = new Font(historyDataGrid.Font.FontFamily, historyDataGrid.Font.Size, FontStyle.Bold);

            historyDataGrid.Columns[nameof(ConvertedValues.Source)].Visible = false;

            foreach (DataGridViewRow row in historyDataGrid.Rows)
            {
                var source = (Source)row.Cells[nameof(ConvertedValues.Source)].Value;
                switch (source)
                {
                    case Source.Unit:
                        row.Cells[nameof(ConvertedValues.Units)].Style = style;
                        break;
                    case Source.Cm:
                        row.Cells[nameof(ConvertedValues.Cm)].Style = style;
                        break;
                    case Source.Inch:
                        row.Cells[nameof(ConvertedValues.Inches)].Style = style;
                        break;
                    case Source.M:
                        row.Cells[nameof(ConvertedValues.M)].Style = style;
                        break;
                    case Source.Foot:
                        row.Cells[nameof(ConvertedValues.Feet)].Style = style;
                        break;
                }
            }
        }
        private void RefreshView(ConvertedValues result)
        {
            unitsTextBox.Text = result.Units.ToString();
            cmTextBox.Text = result.Cm.ToString();
            mTextBox.Text = result.M.ToString();
            inchesTextBox.Text = result.Inches.ToString();
            footTextBox.Text = result.Feet.ToString();
        }

        private void CheckKey(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == '.'))
            { e.Handled = true; }
            TextBox txtDecimal = sender as TextBox;
            if (e.KeyChar == '.' && txtDecimal.Text.Contains("."))
            {
                e.Handled = true;
            }
            if (e.KeyChar == (char)Keys.Enter)
            {
                ConvertValues();
            }
        }

        private void unitsTextBox_Enter(object sender, EventArgs e)
        {
            unitsRadioButton.Checked = true;
        }

        private void cmTextBox_Enter(object sender, EventArgs e)
        {
            cmRadioButton.Checked = true;
        }

        private void mTextBox_Enter(object sender, EventArgs e)
        {
            mRadioButton.Checked = true;
        }

        private void inchesTextBox_Enter(object sender, EventArgs e)
        {
            inchesRadioButton.Checked = true;
        }

        private void footTextBox_Enter(object sender, EventArgs e)
        {
            footRadioButton.Checked = true;
        }

        private void saveHistory_Click(object sender, EventArgs e)
        {
            try
            {
                var list = new List<ConvertedValues>();
                var content = Converter.GetHistoryLog();
                if (content != null)
                {
                    var fileName = $"history_{DateTime.Now.ToString("ddMMyyyy")}.txt";
                    saveFileDialog.FileName = fileName;
                    saveFileDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
                    if (saveFileDialog.ShowDialog() == DialogResult.OK)
                    {
                        using (StreamWriter sw = new StreamWriter(saveFileDialog.FileName))
                            sw.WriteLine(content);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }

        }


        private void selectedFromHistory(object sender, DataGridViewCellEventArgs e)
        {
            var row = historyDataGrid.Rows[e.RowIndex];
            var item = row.DataBoundItem as ConvertedValues;
            RefreshView(new ConvertedValues { Units = item.Units, Cm = item.Cm, M = item.M, Inches = item.Inches, Feet = item.Feet });

        }

        private void copyrightLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(DeveloperUrl);
        }

        private void helpLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(HelpUrl);
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            Converter.History.Clear();
            RefreshGrid();
            TrySecured(() =>
            {
                File.Delete(HistoryPath);
            });
            //RefreshView(new ConvertModel());
        }

        private void MainWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            TrySecured(() =>
            {
                File.WriteAllText(HistoryPath, Converter.History.GetString());
            });
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {
            TrySecured(() =>
            {
                var history = File.ReadAllLines(HistoryPath);
                var result = new List<ConvertedValues>();
                foreach (var line in history)
                {
                    var item = line.Split('|');
                    var values = new ConvertedValues
                    {
                        Units = float.Parse(item[0]),
                        Cm = float.Parse(item[1]),
                        M = float.Parse(item[2]),
                        Feet = float.Parse(item[3]),
                        Inches = float.Parse(item[4]),
                        Source = (Source)Enum.Parse(typeof(Source), item[5])
                    };
                    result.Add(values);

                }
                Converter = new UnitConverter(result);
            });
            RefreshGrid();
        }

        private bool TrySecured(Action action)
        {
            try
            {
                action();
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error: {ex.Message}");
            }
            return true;
        }
    }
}