﻿using Core.Configuration;
using Core.Configuration.Interfaces;
using Core.Logger;
using Inventory.BLL.Managers;
using Inventory.DAL.Constants;
using Inventory.DAL.EntityFramework;
using Inventory.DAL.EntityFramework.DomainModels;
using Inventory.DAL.EntityFramework.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Steam.Api;
using System;
using System.IO;
using System.Linq;

namespace TestConsole
{
	public class DependencyContainer
	{
		private static IServiceProvider _serviceProvider;

		public static void Init()
		{
			var collection = new ServiceCollection();

			collection.AddScoped<DbContext, InventoryDbContext>();


			collection.AddScoped<ILogger, Logger>();
			collection.AddScoped<LogRepository>();
			collection.AddScoped<SettingRepository>();
			collection.AddScoped<PlayerRepository>();
			collection.AddScoped<IInventoryConfiguration, AppConfiguration>();
			collection.AddScoped<SteamApiClient>();
			collection.AddScoped<ItemsManager>();
			collection.AddScoped<PlayerManager>();

			_serviceProvider = collection.BuildServiceProvider();
			DefaultSettings(_serviceProvider.GetService<SettingRepository>());
			var appConfiguration = _serviceProvider.GetService<IInventoryConfiguration>();

			if (!File.Exists(ApplicationConstants.InventoryDbConnectionString))
			{
				var path = Path.GetDirectoryName(ApplicationConstants.InventoryDbConnectionString);
				if (!Directory.Exists(path))
					Directory.CreateDirectory(path);
				File.Create(ApplicationConstants.InventoryDbConnectionString);
			}

			
			var db = _serviceProvider.GetService<DbContext>();
			db.Database.Migrate();
		}

		public static T GetService<T>()
		{
			return _serviceProvider.GetService<T>();
		}

		private static void DisposeServices()
		{
			if (_serviceProvider == null)
			{
				return;
			}
			if (_serviceProvider is IDisposable)
			{
				((IDisposable)_serviceProvider).Dispose();
			}
		}

		private static void DefaultSettings(SettingRepository settingRepository)
		{
			var BaseApiUrl = settingRepository.GetAll().FirstOrDefault(x => x.Name == "BaseApiUrl");
			if (BaseApiUrl == null)
			{
				BaseApiUrl = new Setting { Name = "BaseApiUrl", Value = "http://api.steampowered.com/" };
				settingRepository.Save(BaseApiUrl);
			}

			var BaseSteamUrl = settingRepository.GetAll().FirstOrDefault(x => x.Name == "BaseSteamUrl");
			if (BaseSteamUrl == null)
			{
				BaseSteamUrl = new Setting { Name = "BaseSteamUrl", Value = "https://steamcommunity.com/" };
				settingRepository.Save(BaseSteamUrl);
			}

			var imageSteamUrl = settingRepository.GetAll().FirstOrDefault(x => x.Name == "ImageSteamUrl");
			if (imageSteamUrl == null)
			{
				imageSteamUrl = new Setting { Name = "ImageSteamUrl", Value = "https://steamcommunity.com/economy/image/" };
				settingRepository.Save(imageSteamUrl);
			}
		}
	}
}
