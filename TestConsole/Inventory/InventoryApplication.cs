﻿using Inventory.BLL.Managers;
using Inventory.DAL.EntityFramework;
using Inventory.DAL.EntityFramework.DomainModels;
using Inventory.DAL.EntityFramework.Repositories;
using Inventory.DAL.Models.Response;
using Steam.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestConsole.Inventory
{
	public class InventoryApplication
	{
		private ItemsManager _itemsManager { get; set; }
		private LogRepository Rep { get; set; }
		public InventoryApplication(ItemsManager itemsManager, LogRepository rep)
		{
			_itemsManager = itemsManager;
			Rep = rep;
		}
		public List<string> GetItemNames()
		{
			var items =_itemsManager.GetSchemaItems();
			return items.Select(x => x.Name).ToList();
		}

		public List<PlayerItem> GetPlayerItems(string playerId)
		{
			var items = _itemsManager.GetPlayerCsgoInvenotory(playerId);
			return items?.ToList();
		}

		public void DisplayPlayerItems(string playerId)
		{
			var items = GetPlayerItems(playerId);
			if (items != null)
			{
				Console.WriteLine($"--- The player {playerId} has following items ---");
				foreach (var item in items)
				{
					Console.WriteLine($"--- {item.Name} ---");
				}
				Console.WriteLine($"--- TOTAL {items.Count} ---");

				var first = items.FirstOrDefault(x => x.IsMarketable == true);
				var price = _itemsManager.GetPrice(first.MarketHashName);

				Console.WriteLine($"--- Price for {first.Name}:  {price.ToString()} ---");
			}
			else
			{
				Console.WriteLine($"--- The player {playerId} does not have items ---");
			}
		}

		public void WriteToDbTest()
		{
			Rep.Write(new Log { AdditionalInfo = "add", Level = LogLevels.Debug, Message = "message", Time = DateTime.Now });
		}
	}
}
