﻿using Inventory.BLL.Managers;
using Inventory.DAL.EntityFramework.Repositories;
using System;
using TestConsole.Inventory;

namespace TestConsole
{
	public class Program
	{
		static void Main(string[] args)
		{
			DependencyContainer.Init();
			Inventory.InventoryApplication inApp = new InventoryApplication(DependencyContainer.GetService<ItemsManager>(), DependencyContainer.GetService<LogRepository>());
			inApp.DisplayPlayerItems("the_split");
			//inApp.WriteToDbTest();
			Console.ReadLine();

		}
	}
}
