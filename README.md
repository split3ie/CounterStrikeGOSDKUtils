# SourceSDKUtils

## Hammer Unit Converter 

I made this tool for personal use when I've worked on CS:GO map based by real location so I wanted to have correct values (You can see this map in workshop https://steamcommunity.com/sharedfiles/filedetails/?id=857973374)

With this tool you can easily convert dimensions from Hammer SDK Unit to Cm, M, Inches, Feet and vice versa.
I don't know if this tool will be useful for someone but if will - I'll be happy :)
### Main window

![Main window](https://user-images.githubusercontent.com/7195853/40583774-a51b96ec-619d-11e8-8f82-d6ad2e9b45de.png)

### Generated File

![Generated file](https://user-images.githubusercontent.com/7195853/40583781-d3173a92-619d-11e8-98eb-65aed5cb36f9.png)

#### Options:

* Convertion dimensions;
* View history;
* Save history to file;

Feel free to PM me if you have any questions or proposes.
