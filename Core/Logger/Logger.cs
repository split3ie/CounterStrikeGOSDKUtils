﻿using System;
using System.Linq;

namespace Core.Logger
{
	public class Logger : ILogger
	{
		private NLog.ILogger _logger;
		public Logger()
		{
			var factory = new NLog.LogFactory();
			factory.LoadConfiguration("nlog.config");
			_logger = factory.GetCurrentClassLogger();
		}
		public void LogDebug(string message, params string[] args)
		{
			DoLog(NLog.LogLevel.Debug, message, args);
		}

		public void LogError(string message, params string[] args)
		{
			DoLog(NLog.LogLevel.Error, message, args);
		}

		public void LogException(Exception ex, string message = null, params string[] args)
		{
			args.Append(message);
			DoLog(NLog.LogLevel.Fatal, ex.Message, args);
		}

		public void LogInfo(string message, params string[] args)
		{
			DoLog(NLog.LogLevel.Info, message, args);
		}

		public void LogWarn(string message, params string[] args)
		{
			DoLog(NLog.LogLevel.Warn, message, args);
		}

		private void DoLog(NLog.LogLevel level, string message, params string[] args)
		{
			_logger.Log(level, message);
			if (args != null && args.Any())
			{
				_logger.Log(level, "Additional information:");
				foreach (var arg in args)
				{
					_logger.Log(level, arg);
				}
			}
		}
	}
}
