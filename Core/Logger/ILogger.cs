﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Logger
{
	public interface ILogger
	{
		void LogInfo(string message, params string[] args);
		void LogWarn(string message, params string[] args);
		void LogException(Exception ex, string message = null, params string[] args);
		void LogDebug(string message, params string[] args);
		void LogError(string message, params string[] args);
	}
}
