﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Configuration.Interfaces
{
    public interface IInventoryConfiguration
    {
        string ApiKey { get; }
		string BaseApiUrl { get; }
		string BaseSteamUrl { get; }
	}
}
