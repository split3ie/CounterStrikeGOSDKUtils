﻿using Core.Configuration.Interfaces;
using Core.Logger;
using Inventory.DAL.Interfaces;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

namespace Steam.Api
{
	public class SteamApiClient
	{
		private readonly IInventoryConfiguration _configuration;
		private string _baseUrl;
		private readonly ILogger _logger;
		public SteamApiClient(IInventoryConfiguration configuration, ILogger logger)
		{
			_baseUrl = configuration.BaseApiUrl;
			_configuration = configuration;
			_logger = logger;
		}
		/// <summary>
		/// Send Rest request and get result
		/// </summary>
		/// <typeparam name="T">Request</typeparam>
		/// <typeparam name="U">Response</typeparam>
		/// <param name="request">Request model</param>
		/// <returns>Response of Rest request</returns>
		public U GetResult<T, U>(T request) where T : IApiRequest
		{
			if (request.UseSteamUrl)
				ChangeBaseUrl(_configuration.BaseSteamUrl);

			var client = new RestSharp.RestClient(_baseUrl);
			var restRequest = new RestSharp.RestRequest(request.Resource, (RestSharp.Method)request.Method);
			if (restRequest.Method == RestSharp.Method.POST)
				restRequest.AddJsonBody(request);

			var response = client.Execute(restRequest);
			if (response.IsSuccessful && response.StatusCode == System.Net.HttpStatusCode.OK)
			{
				try
				{
					var parsedContent = JObject.Parse(response.Content);
					var content = parsedContent.SelectToken("result");
					if (content != null)
						return JsonConvert.DeserializeObject<U>(content.ToString());
					else
						return JsonConvert.DeserializeObject<U>(response.Content);
				}
				catch (Exception ex)
				{
					_logger.LogException(ex, "Error on parsing result", response.Content);
				}

			}
			else
			{
				_logger.LogError($"The api return status {response.StatusCode.ToString()}");
			}
			return default(U);
		}

		private void ChangeBaseUrl(string baseUrl)
		{
			_baseUrl = baseUrl;
		}
	}
}
