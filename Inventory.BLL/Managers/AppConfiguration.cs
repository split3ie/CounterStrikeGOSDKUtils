﻿using Core.Configuration.Interfaces;
using Inventory.DAL.EntityFramework.Repositories;

namespace Core.Configuration
{
	public class AppConfiguration : IInventoryConfiguration
	{
		#region Setting Keys
		private const string APIKEY = "ApiKey";
		private const string BASEAPIURL = "BaseApiUrl";
		private const string BASESTEAMURL = "BaseSteamUrl";
		#endregion

		public string ApiKey { get; }
		public string BaseApiUrl { get; }

		public string BaseSteamUrl { get; }

		public AppConfiguration(SettingRepository settingRepository)
		{
			ApiKey = settingRepository.GetString(APIKEY);
			BaseApiUrl = settingRepository.GetString(BASEAPIURL);
			BaseSteamUrl = settingRepository.GetString(BASESTEAMURL);
		}
	}
}
