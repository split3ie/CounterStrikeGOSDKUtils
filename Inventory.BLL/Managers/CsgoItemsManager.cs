﻿using Core.Configuration.Interfaces;
using Core.Logger;
using Inventory.DAL;
using Inventory.DAL.EntityFramework.DomainModels;
using Inventory.DAL.Models.Request;
using Inventory.DAL.Models.Response;
using Steam.Api;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Inventory.BLL.Managers
{
	public class ItemsManager : BaseManager
	{
		private readonly SteamApiClient _apiClient;
		private readonly IInventoryConfiguration _configuration;
		private readonly PlayerManager _playerManager;

		public ItemsManager(SteamApiClient apiClient, IInventoryConfiguration configuration, PlayerManager playerManager, ILogger logger) : base(logger)
		{
			_apiClient = apiClient;
			_configuration = configuration;
			_playerManager = playerManager;
		}

		public IList<MarketItem> GetSchemaItems()
		{
			_logger.LogInfo("Trying to get Schema items");

			var result = DoSafe(() =>
		   {
			   var request = new ItemSchemaRequest(_configuration.ApiKey);
			   return _apiClient.GetResult<ItemSchemaRequest, ItemsSchemeResponse>(request);
		   });

			if (!result.Status)
			{
				_logger.LogWarn("Schema items return - false");
				return null;
			}

			_logger.LogInfo($"Found {result.Items.Count} Schema items");
			return result.Items;
		}

		public List<PlayerItem> GetPlayerCsgoInvenotory(string playerId)
		{
			var player = _playerManager.GetPlayerInformation(playerId);
			if (player != null)
				return player.Items;

			_logger.LogInfo($"Trying to get CS:GO inventory for player {playerId}");
			var request = new PlayerCsgoInventoryRequest(playerId);
			var result = _apiClient.GetResult<PlayerCsgoInventoryRequest, PlayerCsgoInventoryResponse>(request);
			if (result == null || !result.Success)
			{
				_logger.LogWarn($"Can't retrive CS:GO inventory for {playerId}");
				return null;
			}
			_logger.LogInfo($"Found {result.Details.Count} CS:GO items for {playerId}");

			player = new Player { ProfileId = playerId, LastModified = DateTime.Now, Items = new List<PlayerItem>() };
			foreach (var item in result.Details.Select(x => x.Value))
				player.Items.Add(new PlayerItem
				{
					Amount = result.Inventory.FirstOrDefault(x => x.Value.ClassId == item.ClassId).Value?.Amount ?? 0,
					ClassId = item.ClassId,
					ApplicationId = 730,
					IconUrl = item.IconUrl,
					IsCommodity = item.IsCommodity,
					IsMarketable = item.IsMarketable,
					IsTradable = item.IsTradable,
					MarketHashName = item.MarketHashName,
					Name = item.Name,
					MarketTradableRestriction = item.MarketTradableRestriction,
					Player = player,
					Type = item.Type
				});

			_playerManager.SavePlayer(player);
			return player.Items;
		}

		public MarketPriceResponse GetPrice(string marketHashName, Currency currency = Currency.USD)
		{
			_logger.LogInfo($"Trying to get price for {marketHashName}");

			var result = DoSafe(() =>
			{
				var request = new MarketPriceRequest((int)currency, marketHashName);
				return _apiClient.GetResult<MarketPriceRequest, MarketPriceResponse>(request);
			});

			if (!result.Success)
			{
				_logger.LogWarn($"Can't retrive price for {marketHashName}");
				return null;
			}

			_logger.LogInfo($"Found price for {marketHashName}");
			return result;
		}
	}
}
