﻿using Core.Logger;
using System;

namespace Inventory.BLL.Managers
{
	public abstract class BaseManager
	{
		protected readonly ILogger _logger;
		public BaseManager(ILogger logger)
		{
			_logger = logger;
		}

		public virtual TResult DoSafe<TResult>(Func<TResult> action)
		{
			try
			{
				return action();
			}
			catch (Exception ex)
			{
				_logger.LogException(ex, $"Exception for {action.Method}");
			}
			return default(TResult);
		}
	}
}