﻿using Inventory.DAL.EntityFramework.DomainModels;
using Inventory.DAL.EntityFramework.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Inventory.BLL.Managers
{
	public class PlayerManager
	{
		private readonly PlayerRepository _playerRepository;
		public PlayerManager(PlayerRepository playerRepository)
		{
			_playerRepository = playerRepository;
		}

		public Player GetPlayerInformation(string profileId)
		{
			return _playerRepository.GetPlayer(profileId);
		}

		public void SavePlayer(Player player)
		{
			_playerRepository.Save(player);
		}
	}
}
